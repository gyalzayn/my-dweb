    <!-- Start: FOOTER -->
    <footer>
      <div class="container">
        <div class="row">
          <div class="span2">
            <h4><i class="icon-star icon-white"></i> Events</h4>
            <nav>
              <ul class="quick-links">
                <li><a href="all_products.php">All Events</a></li>
              </ul>
            </nav>
            <h4><i class="icon-cogs icon-white"></i> Blogs</h4>
            <nav>
              <ul class="quick-links">
                <li><a href="all_services.php">All Blogs</a></li>              
              </ul>
            </nav>
          </div>
          <div class="span2">
            <h4><i class="icon-beaker icon-white"></i> About</h4>
            <nav>
              <ul class="quick-links">
                <li><a href="events.php">Events</a></li>
                <li><a href="blog.php">Blog</a></li>
                </ul>
            </nav>          
          </div>
          <div class="span2">
            <h4><i class="icon-thumbs-up icon-white"></i> Support</h4>
            <nav>
              <ul class="quick-links">
                <li><a href="contact_us.php">Contact Us</a></li>
            </ul>
            </nav>           
          </div>
          <div class="span3">
            <h4>&nbsp;Get in touch</h4>
            <div class="social-icons-row">
              <a href="#"><i class="icon-twitter"></i></a>
              <a href="#"><i class="icon-facebook"></i></a>
              <a href="#"><i class="icon-linkedin"></i></a>                                         
            </div>
            <div class="social-icons-row">
              <a href="#"><i class="icon-google-plus"></i></a>              
              <a href="#"><i class="icon-github"></i></a>
              <a href="mailto:lulworthcovedramaclub@gmail.com"><i class="icon-envelope"></i></a>        
            </div>
          </div>      
          <div class="span3">
            <h4>Get updated by email</h4>
            <form method="post" action="getMessage.php">
              <input type="text" name="email" placeholder="Email address">
              <input type="submit" class="btn btn-primary" value="Subscribe" name="getEmail">
            </form>
          </div>
      </div>
      <hr class="footer-divider">
      <div class="container">
        <p>
          &copy; 2018 Lulworth Cove Drama Club , Inc. All Rights Reserved.
        </p>
      </div>
          </div>
    </footer>
    <!-- End: FOOTER -->
    <script type="text/javascript" src="js/jquery.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/boot-business.js"></script>
  </body>
</html>