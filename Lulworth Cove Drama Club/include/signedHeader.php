<?php
if($_SESSION['login']='yes'){
    ?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Lulworth Cove Drama Club are a local community group and they focus on developing local talent through vocational courses for adults and children.  Children between the ages of 5-21 can attend any of the youth courses and those over 21 join the adults either on a full-time or part-time basis.">
    <meta name="author" content="DawaGyalzenSherpa">
    <title>LCDC | Lulworth Cove Drama Club</title>
    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <!-- Bootstrap responsive -->
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet" type="text/css">
    <!-- Font awesome - iconic font with IE7 support --> 
    <link href="css/font-awesome.css" rel="stylesheet" type="text/css">
    <link href="css/font-awesome-ie7.css" rel="stylesheet" type="text/css">
    <!-- Bootbusiness theme -->
    <link href="css/boot-business.css" rel="stylesheet" type="text/css">
    <!-- Custom CSS -->
    <link href="css/custom.css" rel="stylesheet" type="text/css">
  </head>
<body>
    <!-- Start: HEADER -->
    <header>
      <!-- Start: Navigation wrapper -->
      <div class="navbar navbar-fixed-top">
        <div class="navbar-inner">
          <div class="container">
            <a href="index.php" class="brand brand-bootbus">Lulworth Cove Drama Club</a>
            <!-- Below button used for responsive navigation -->
            <button type="button" class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <!-- Start: Primary navigation -->
            <div class="nav-collapse collapse">        
              <ul class="nav pull-right">
                <li><a href="profile.php" class="active-link">Profile</a></li>
                <li><a href="forum.php">Forum</a></li>
                <li><a href="events.php">Events</a></li>
                <li><a href="blog.php">Blog</a></li>
                <li><a href="signout.php">Signout</a></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
      <!-- End: Navigation wrapper -->   
    </header>
<!-- End: HEADER -->
    <?php
}
?>