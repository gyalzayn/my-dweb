<?php include_once("include/adminHeader.php");
include_once("dbConnect.php");

if(isset($_POST['submit'])){
    $image = $_FILES['photo']['name'];
    $tname = $_FILES['photo']['tmp_name'];
    $name = $_POST['name'];
    $content = $_POST['content'];
    $type = $_POST['type'];
    $author = $_POST['author'];
    $date = date("Y-m-d");
    
    $query = "INSERT INTO tbl_blog values('','$image', '$name', '$content', '$type', '$author', '$date')";
    if($conn->query($query)){
        move_uploaded_file($tname, "img/events/".$image);
        echo "New Blog Added";
    } else {
        die("Something went wrong");
    }
}

?>

<!-- Start: MAIN CONTENT -->
    <div class="content">
      <div class="container">
        <div class="page-header">
          <h1>Manage LCDC Events</h1>
        </div>
        <div class="row">
          <div class="span6 offset3">
            <h4 class="widget-header"><i class="icon-gift"></i> Add New Blog</h4>
            <div class="widget-body">
              <div class="center-align">
                <form method="post" class="form-horizontal form-manageBlog">
                    <input type="file" name="photo" id="photo" required="">
                    <input type="text" name="name" placeholder="Name of Event">
                    <textarea name="content" placeholder="Content of Event" required=""></textarea>
                    <input type="text" name="type" placeholder="Type of Event" required="">
                    <input type="text" name="author" placeholder="Name of Author" required="">
                  <div>
                    <input type="submit" name="submit" class="btn btn-primary btn-large" value="SUBMIT">
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
<!-- End: MAIN CONTENT -->


<?php include_once("include/footer.php"); ?>