<?php include_once("include/header.php"); 
require 'dbConnect.php';
if(isset($_POST['signup']))
{
    $name=$_POST['fullname'];
    $email=$_POST['email'];
    $dob=$_POST['bday'];
    $today = date("Y-m-d"); 
    $age = date_diff($dob,$today); // Age Calculator
    $address=$_POST['address'];
    $username=$_POST['username'];
    $password=($_POST['password_confirmed']);
    $type = 'user';
    $class =$_POST['class'];

    $register="insert into tbl_register values('','$name','$email','$dob',' $age', '$address','$username','$password','$type','$today')";

    if($conn->query($register) == false)
    {
    die('Error'.$conn->connect_error);
    } else {
    echo '<h3>User Registered</h3>';
    }
    
    $query = "select * from tbl_register where username='$username'";
    $result = $conn->query($query);
    $num = $result->num_rows;
    if($num==1){
        
    $row = $result->fetch_array();
    $user_id = $row['id'];
    $class = "insert into tbl_class
    values('', '$class', '$user_id')";
        
    if($conn->query($class) == false)
    {
        die('Error'.$conn->connect_error);   
    } else {
        echo '<h3>Class Registered</h3>';
    }
    }
}
  ?>
<!-- Start: MAIN CONTENT -->
    <div class="content">
      <div class="container">
        <div class="page-header">
          <h1>Signup to Lulworth Cove Drama Club</h1>
        </div>
        <div class="row">
          <div class="span6 offset3">
            <h4 class="widget-header"><i class="icon-gift"></i> Be a part of LCDC</h4>
            <div class="widget-body">
              <div class="center-align">
                <form method="post" class="form-horizontal form-signin-signup">
                    <input type="text" name="fullname" placeholder="Full Name">
                    <input type="email" name="email" placeholder="Email">
                    <input type="date" name="bday" placeholder="Select Your Date of Birth">
                    <input type="text" name="address" placeholder="Address">
                    <select name="class">
                        <option value="dancing">Dancing</option>
                        <option value="singing">Singing</option>
                        <option value="drama">Drama</option>
                    </select>
                    <input type="text" name="username" placeholder="Username" onkeyup="user_check()">
                    <input type="password" name="password" placeholder="Password">
                    <input type="password" name="password_confirmed" placeholder="Password Confirmation">
                  <div>
                    <input type="submit" name="signup" class="btn btn-primary btn-large" value="SUBMIT">
                  </div>
                </form>
                <h4><i class="icon-question-sign"></i> Already have an account?</h4>
                <a href="signin.php" class="btn btn-large bottom-space">Signin</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
<!-- End: MAIN CONTENT -->
<script>  
      function user_check()
      {
	      var user = document.getElementById("username").value;
	      var req;
	      if(window.XMLHttpRequest){
	      	req = new XMLHttpRequest();
	      } else {
	      	req = new ActiveXObject("Microsoft.XMLHTTP");
	      }

	      req.onreadystatechange=function()
	      {
	      	if (req.readyState==4 && req.status==200){
	        	document.getElementById("msg").innerHTML = req.responseText;
	        
	        if(req.responseText == 1){
	        	alert("Username Exists");
	        }
	    
	        }
	      }
	      req.open("GET","ajax.php?value="+user,true);
	      req.send();
      }
</script>
<?php include_once("include/footer.php");  ?>