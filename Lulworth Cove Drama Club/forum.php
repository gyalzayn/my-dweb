<?php
include('dbConnect.php');
session_start();
$uid = $_SESSION['uid'];
$user = $_SESSION['account'];
$type = $_SESSION['type'];

if($_SESSION['login']='yes') {
    include_once("include/signedHeader.php");
} else {
include_once("include/header.php");  
}

if(isset($_POST['reply-submit'])){
	$que_id = $_POST['qid'];
	$ureply = $_POST['user-reply'];
	$query_postAns = "INSERT INTO tbl_answers(id, question_id, answer, uid, user)VALUES('','$que_id','$ureply','$uid','$user')";
	if($conn->query($query_postAns)==false){
		echo "Something went wrong!";
	} else {
		header('location: forum.php');
	}
}

if(isset($_GET['question-submit'])){
	$question = $_GET['question'];

	$query_postQ = "insert into tbl_questions(id,uid,question,user) values('','$uid','$question','$user')";
	if($conn->query($query_postQ)==false){
		echo "Error:".$con->error;
	} else {
		header('location: forum.php');
		}
	}

if(isset($_POST['delete-reply'])){
	$ansid = $_POST['ansid'];
	$query_deteleReply = "delete from tbl_answers WHERE id='$ansid'";
	print_r($query_deteleReply);
	if($conn->query($query_deteleReply)==false){
		echo "Error: ".$con->error;
	} else {
		header('location: forum.php');
		}
	}
?>
<!-- Start: MAIN CONTENT -->
    <div class="content">
      <div class="container">
        <div class="page-header">
          <h1>Our Online Forum</h1>
        </div>
        <div class="row-fluid">
            <ul class="thumbnails">
                <li class="span5">
                    <h4 class="widget-header">Clear your doubts here.</h4>
                    <div class="center-align">
                    <form>
                        <textarea name="question" placeholder="ASK" required=""></textarea>
                        <br/>
                        <input type="submit" value="POST" name="question-submit" class="btn btn-primary btn-large">
                    </form>
                    </div>
                </li>
                <li class="span7">
                    <h4 class="widget-header">Answers</h4>
                    <div class="center-align">
                    <?php 
                        $start = 0;
                        $end = 4;
                    
                        $query = "select * from tbl_questions";
                        $result = $conn->query($query);
                        $total = $result->num_rows;
                    
                        $pages = ceil($total/$end);
                    
                        if(isset($_GET['pg'])){
                            $start = $end * ($_GET['pg']-1);
                        }
                    
                    $query_Quest = "SELECT * FROM tbl_questions ORDER BY id DESC LIMIT $start, $end";
                    $result = $conn->query($query_Quest);
                    if($result->num_rows > 0){
                        while($row = $result->fetch_assoc()){
                            $qid = $row['id'];
                            echo "<div class='que-ans'>";
                            echo "<div class='question'>".$row['question']."</div><div class='askedby'> asked by <strong>".$row['user']."</strong></div><div class='reply'></div>";
                            
                            echo "<div id='reply-form'>
							<form method='post'>
                            <br/>
                                <input type='hidden' value='$qid' name='qid'>
								<textarea name='user-reply' placeholder='Reply to: ".$row['question']."' required='required'></textarea>
                                <br/>
								<button name='reply-submit' class='btn btn-primary btn-large'>Answer</button>
							</form>
						</div>";
                            
                            echo "<div class='replies-text'>Replies :</div>";
                            echo "<div class='replies'>";
                            
                            $query_Ans = "SELECT * FROM tbl_answers WHERE question_id='$qid'";
                            $result = $conn->query($query_Ans);
                            while($ansrow = $result->fetch_assoc()){
                                echo "<div class='reply-section'>";
                                
                                echo "<div class='answer'>".$ansrow['answer']."</div><div class='ansby'> answered by <strong>".$ansrow['user']."</strong>";
                                if($uid==$ansrow['uid']){
                                    echo "<form method='post'>
                                    <input type='text' name='ansid' value=".$ansrow['id']." hidden>
                                    <button name='delete-reply' class='btn btn-danger btn-large'>Delete</button></form>";
                                }
                                echo "</div></div>";
                            }
                            echo "</div></div>";
                        }
                    }
                    ?>
                    </div>  
                </li>
            </ul>
        </div>
      </div>
    </div>
<!-- End: MAIN CONTENT -->
<?php include_once("include/footer.php");  ?>