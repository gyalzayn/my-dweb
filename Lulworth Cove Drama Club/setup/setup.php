<form method="post">
    Click<button name="submit">Proceed</button>to set up the database.<br/>
</form>
<?php
if(isset($_POST['submit'])){
    $conn = new mysqli("localhost", "root", "");
    $query_db = "CREATE DATABASE IF NOT EXITS `spring_assignment`";
    
    if($conn->query($query_db)){
        echo "Database `spring_assignment` successfully created <br/>";
        $conn = new mysqli("localhost", "root", "", "spring_assignment");
        echo "Database `spring_assignment` connected <br/>";
    } else {
        echo "Error while creating database.".$conn->error_connect;
    }
    
    // 1 - Table structure for table `tbl_register`
    $register = "CREATE TABLE `tbl_register`(
        `id` int(3) NOT NULL AUTO_INCREMENT,
        `fullname` varchar(50) NOT NULL,
        `email` varchar(50) NOT NULL,
        `dob` date NOT NULL,
        `age` int(3) NOT NULL,
        `address` varchar(80) NOT NULL,
        `username` varchar(15) NOT NULL,
        `password` varchar(15) NOT NULL,
        `type` varchar(20) NOT NULL,
        `last_login` date NOT NULL,
        PRIMARY KEY (id),
        UNIQUE(username)
    )";
    if($conn->query($register)){
        echo "Table 'tbl_register' created successfully! <br/>";
    } else {
        echo "Error: ".$conn->error. "<br/>";
    }
    
    // 2 - Table structure for table `tbl_questions`
    $questions = "CREATE TABLE `tbl_questions`(
        `id` int(11) NOT NULL AUTO_INCREMENT,
        `uid` int(11) NOT NULL,
        `question` varchar(500) NOT NULL,
        `user` varchar(500) NOT NULL,
        PRIMARY KEY (id)
    )";
    if($conn->query($questions)){
        echo "Table 'tbl_questions' created successfully! <br/>";
    } else {
        echo "Error: ".$conn->error. "<br/>";
    }
    
    // 3 - Table structure for table `tbl_answers`
    $answer = "CREATE TABLE `tbl_answers`(
        `id` int(11) NOT NULL AUTO_INCREMENT,
        `question_id` int(11) NOT NULL,
        `answer` varchar(600) NOT NULL,
        `uid` int(11) NOT NULL,
        `user` varchar(40) NOT NULL,
        PRIMARY KEY (id)
    )";
    if($conn->query($answer)){
        echo "Table 'tbl_answers' created successfully! <br/>";
    } else {
        echo "Error: ".$conn->error. "<br/>";
    }
    
    // 4 - Table structure for table `tbl_events`
    $events = "CREATE TABLE `tbl_events`(
        `id` int(11) NOT NULL AUTO_INCREMENT,
        `image` varchar(150) NOT NULL,
        `name` varchar(50) NOT NULL,
        `content` varchar(5000) NOT NULL,
        `type` varchar(50) NOT NULL,
        `date_registered` date NOT NULL,
        PRIMARY KEY (id)
    )";
    if($conn->query($events)){
        echo "Table 'tbl_events' created successfully! <br/>";
    } else {
        echo "Error: ".$conn->error. "<br/>";
    }
    
    // 5 - Table structure for table `tbl_blog`
    $blog = "CREATE TABLE `tbl_blog`(
        `id` int(11) NOT NULL AUTO_INCREMENT,
        `image` varchar(150) NOT NULL,
        `name` varchar(20) NOT NULL,
        `content` varchar(5000) NOT NULL,
        `type` varchar(50) NOT NULL,
        `author` varchar(20) NOT NULL,
        `date_registered` date NOT NULL,
        PRIMARY KEY (id)
    )";
    if($conn->query($blog)){
        echo "Table 'tbl_blog' created successfully! <br/>";
    } else {
        echo "Error: ".$conn->error. "<br/>";
    }
    
    // 6 - Table structure for table `tbl_class`
    $class = "CREATE TABLE `tbl_class`(
        `id` int(1) NOT NULL AUTO_INCREMENT,
        `name` varchar(10) NOT NULL,
        `user_id` int(3) NOT NULL,
        PRIMARY KEY (id)
    )";
    if($conn->query($class)){
        echo "Table 'tbl_class' created successfully! <br/>";
    } else {
        echo "Error: ".$conn->error. "<br/>";
    }
    
    // 7 - Table structure for table `tbl_news`
    $news = "CREATE TABLE `tbl_news`(
        `id` int(11) NOT NULL AUTO_INCREMENT,
        `email` varchar(50) NOT NULL,
        PRIMARY KEY (id)
    )";
    if($conn->query($news)){
        echo "Table 'tbl_news' created successfully! <br/>";
    } else {
        echo "Error: ".$conn->error. "<br/>";
    }
    
    // 8 - Calculation of age
    
    ?>