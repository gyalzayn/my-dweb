<?php
if($_SESSION['login']='yes') {
    include_once("include/signedHeader.php");
} else {
include_once("include/header.php");  
}
require 'dbConnect.php';

$query = "SELECT * FROM tbl_blog";
$result = $conn->query($query);
$data = $result->fetch_array();

?>
<!-- Start: MAIN CONTENT -->
    <div class="content">
      <div class="container">
        <div class="page-header">
          <h1>The Bootbusiness Blog</h1>
        </div>
        <div class="row">
          <div class="span12">
            <article class="post-row article">
              <h3>
                <a href="post.html">Awesome post-row tilte goes here</a>
                <small>by username on October 2, 2012</small>
              </h3>
              <img src="http://placehold.it/650x300" class="thumbnail bottom-space">
              <p>
                Post content.Post content.Post content.Post content.
                Post content.Post content.Post content.Post content.
                Post content.Post content.Post content.Post content.
                Post content.Post content.Post content.Post content.
                Post content.Post content.Post content.Post content.
                Post content.Post content.Post content
              </p>
              <p>
                Post content.Post content.Post content.
                Post content.Post content.Post content.Post content.
                Post content.Post content.Post content.Post content.
                Post content.Post content.Post content.Post content.
                Post content.Post content.Post content.Post content.
                Post content.Post content.Post content.Post content.
              </p>
            </article>
            <article class="post-row article">
              <h3>
                <a href="post.html">Awesome post-row tilte goes here</a>
                <small>by username on October 2, 2012</small>
              </h3>
              <img src="http://placehold.it/650x300" class="thumbnail bottom-space">
              <p>
                Post content.Post content.Post content.Post content.
                Post content.Post content.Post content.Post content.
                Post content.Post content.Post content.Post content.
                Post content.Post content.Post content.Post content.
                Post content.Post content.Post content.Post content.
                Post content.Post content.Post content
              </p>
              <p>
                Post content.Post content.Post content.
                Post content.Post content.Post content.Post content.
                Post content.Post content.Post content.Post content.
                Post content.Post content.Post content.Post content.
                Post content.Post content.Post content.Post content.
                Post content.Post content.Post content.Post content.
              </p>
            </article>
            <article class="post-row article">
              <h3>
                <a href="post.html">Awesome post-row tilte goes here</a>
                <small>by username on October 2, 2012</small>
              </h3>
              <img src="http://placehold.it/650x300" class="thumbnail bottom-space">
              <p>
                Post content.Post content.Post content.Post content.
                Post content.Post content.Post content.Post content.
                Post content.Post content.Post content.Post content.
                Post content.Post content.Post content.Post content.
                Post content.Post content.Post content.Post content.
                Post content.Post content.Post content
              </p>
              <p>
                Post content.Post content.Post content.
                Post content.Post content.Post content.Post content.
                Post content.Post content.Post content.Post content.
                Post content.Post content.Post content.Post content.
                Post content.Post content.Post content.Post content.
                Post content.Post content.Post content.Post content.
              </p>
            </article>
            <article class="post-row article">
              <h3>
                <a href="post.html">Awesome post-row tilte goes here</a>
                <small>by username on October 2, 2012</small>
              </h3>
              <img src="http://placehold.it/650x300" class="thumbnail bottom-space">
              <p>
                Post content.Post content.Post content.Post content.
                Post content.Post content.Post content.Post content.
                Post content.Post content.Post content.Post content.
                Post content.Post content.Post content.Post content.
                Post content.Post content.Post content.Post content.
                Post content.Post content.Post content
              </p>
              <p>
                Post content.Post content.Post content.
                Post content.Post content.Post content.Post content.
                Post content.Post content.Post content.Post content.
                Post content.Post content.Post content.Post content.
                Post content.Post content.Post content.Post content.
                Post content.Post content.Post content.Post content.
              </p>
            </article>
            <article class="post-row article">
              <h3>
                <a href="post.html">Awesome post-row tilte goes here</a>
                <small>by username on October 2, 2012</small>
              </h3>
              <img src="http://placehold.it/650x300" class="thumbnail bottom-space">
              <p>
                Post content.Post content.Post content.Post content.
                Post content.Post content.Post content.Post content.
                Post content.Post content.Post content.Post content.
                Post content.Post content.Post content.Post content.
                Post content.Post content.Post content.Post content.
                Post content.Post content.Post content
              </p>
              <p>
                Post content.Post content.Post content.
                Post content.Post content.Post content.Post content.
                Post content.Post content.Post content.Post content.
                Post content.Post content.Post content.Post content.
                Post content.Post content.Post content.Post content.
                Post content.Post content.Post content.Post content.
              </p>
            </article>
            <div class="pagination pagination-centered">
              <ul>
                <li class="disabled">
                  <a href="#">&laquo;</a>
                </li>
                <li class="active">
                  <a href="#">1</a>
                </li>
                <li>
                  <a href="#">2</a>
                </li>
                <li>
                  <a href="#">3</a>
                </li>
                <li>
                  <a href="#">4</a>
                </li>
                <li>
                  <a href="#">5</a>
                </li>
                <li>
                  <a href="#">6</a>
                </li>
                <li>
                  <a href="#">&raquo;</a>
                </li>
              </ul>
            </div>
          </div>       
        </div>
      </div>
    </div>
<!-- End: MAIN CONTENT -->
<?php include_once("include/footer.php");  ?>