<?php include_once("include/header.php"); ?>
<?php
require 'dbConnect.php';

if(isset($_POST['signin'])){
$username = $_POST['username'];
$password = ($_POST['password']);

if(isset($_COOKIE[$username])){
    if($_COOKIE[$username]>=3){
        die("You are blocked for 5 minutes.");
    }
}

$query = "SELECT * FROM tbl_register WHERE username = '$username' AND password = '$password'";

$result = $conn->query($query);
$num = $result->num_rows;
if($num==1){
    session_start();
        $row = $result->fetch_assoc();
        $id = $row['id'];    
    
        $_SESSION['login']='yes';   
        $_SESSION['uid'] = $row['id'];
        $_SESSION['account'] = $row['username'];
        $_SESSION['type'] = $row['type'];
        $today = date("Y-m-d");
    
        $update = "UPDATE tbl_register set last_login = '$today' where id = '$id'";
        $conn->query($update);
    
        $lastVisit = $row['last_login'];
        $today = strtotime($today);
        $last = strtotime($lastVisit);
    
        $inactive = ($today-$last) / (60*60*30*24);
    
        if(inactive >= 24){
            $user_delete = "DELETE FROM tbl_register WHERE username = '$username'";
            $conn->query($user_delete);
            die("Your account has been deleted as you have been inactive for 24 months.");
        }
    
        if($_SESSION['type']=="admin"){
            header('Location: adminIndex.php');
        } else if($_SESSION['type']=="user"){
            header('Location: class.php');
        }
    } else {
    setcookie($username, 1);
    if(isset($_COOKIE[$username])){
        if($_COOKIE[$username] < 3){
            setcookie($username, $_COOKIE[$username]+1, time()+300);
        }
        echo "<script>alert ('Invalid Username or Password!<br/> You have '+(3-$_COOKIE[$username])+' attempts left.')</script>";
    }
}
}
?>
<!-- Start: MAIN CONTENT -->
<div class="content">
      <div class="container">
        <div class="page-header">
          <h1>Signin to LCDC</h1>
        </div>
        <div class="row">
          <div class="span6 offset3">
            <h4 class="widget-header"><i class="icon-lock"></i> Signin to LCDC</h4>
            <div class="widget-body">
              <div class="center-align">
                <form class="form-horizontal form-signin-signup" name="signin" method="post">
                  <input type="text" name="username" placeholder="Username">
                  <input type="password" name="password" placeholder="Password">
                  <div class="remember-me">
                    <div class="pull-left">
                      <label class="checkbox">
                        <input type="checkbox"> Remember me
                      </label>
                    </div>
                    <div class="pull-right">
                      <a href="#">Forgot password?</a>
                    </div>
                    <div class="clearfix"></div>
                  </div>
                  <input type="submit" value="Signin" name="signin" class="btn btn-primary btn-large">
                </form>
                <h4><i class="icon-question-sign"></i> Don't have an account?</h4>
                <a href="signup.php" class="btn btn-large bottom-space">Signup</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
<!-- End: MAIN CONTENT -->
<?php include_once("include/footer.php");  ?>