<?php
include_once('dbConnect.php');
session_start();

if(!isset($_SESSION['account'])){
    if(session_unset()){
        header('Location: index.php');
    }
} else {
    $username = $_SESSION['account'];
    $query = "select * from tbl_register where username = '$username'";
    $result = $conn->query($query);
    $row = $result->fetch_array();
    
    if(isset($_POST['updateProfile'])){
        $name = $_POST['fullname'];
        $email = $_POST['email'];
        $dob = $_POST['bday'];
        $address = $_POST['address'];
        
        $update = "update tbl_register set fullname='$name', email='$email', dob='$dob', address= '$address' where username='$username'";
        
        if($conn->query($update)){
            echo "<script> alert('Details Updated Successfully!')</script>";
            header('Location:profile.php');
        } else {
            die("<script> alert('".$conn->error."')</script>");
        }
    }
}

if($_SESSION['login']='yes') {
    include_once("include/signedHeader.php");
} else {
include_once("include/header.php");  
}
?>
<!-- Start: MAIN CONTENT -->
    <div class="content">
      <div class="container">
        <div class="page-header">
          <h1>Your Profile</h1>
        </div>
<div class="row-fluid">
    <ul class="thumbnails">
          <li class="span6">
            <h4 class="widget-header">Your Current Profile</h4>
            <div class="widget-body">
              <div class="center-align">
                <p><span>Full Name:</span><?php echo $row['fullname'] ?></p>
                  <p><span>Email: </span><?php echo $row['email'] ?></p>
                  <p><span>Date of Birth: </span><?php echo $row['dob'] ?></p>
                  <p><span>Address: </span><?php echo $row['address'] ?></p>
                  <p><span>Username: </span><?php echo $row['username'] ?></p>
              </div>
            </div>
              </li>
                  <li class="span6">
            <h4 class="widget-header"> Update Your Profile</h4>
            <div class="widget-body">
              <div class="center-align">
                <form method="post" class="form-horizontal form-signin-signup">
                    <input type="text" name="fullname" required="" placeholder="New Full Name" value="<?php echo $row['fullname']; ?>">
                    <input type="email" required="" name="email" placeholder="NEw Email" value="<?php echo $row['email']; ?>">
                    <input type="date" name="bday" placeholder="Select Your Date of Birth" required="" value="<?php echo $row['dob']; ?>">
                    <input type="text" name="address" placeholder="Address" required="" value="<?php echo $row['address']; ?>">
                  <div>
                    <input type="submit" name="updateProfile" class="btn btn-primary btn-large" value="UPDATE PROFILE">
                  </div>
                </form>
              </div>
            </div>
              </li>
    </ul>
          </div>
        </div>
      </div>
<!-- End: MAIN CONTENT -->
<?php include_once("include/footer.php");  ?>