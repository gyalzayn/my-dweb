<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Lulworth Cove Drama Club are a local community group and they focus on developing local talent through vocational courses for adults and children.  Children between the ages of 5-21 can attend any of the youth courses and those over 21 join the adults either on a full-time or part-time basis.">
    <meta name="author" content="DawaGyalzenSherpa">
    <title>LCDC | Admin's Dashboard</title>
    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <!-- Bootstrap responsive -->
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet" type="text/css">
    <!-- Font awesome - iconic font with IE7 support --> 
    <link href="css/font-awesome.css" rel="stylesheet" type="text/css">
    <link href="css/font-awesome-ie7.css" rel="stylesheet" type="text/css">
    <!-- Bootbusiness theme -->
    <link href="css/boot-business.css" rel="stylesheet" type="text/css">
    <!-- Custom CSS -->
    <link href="css/custom.css" rel="stylesheet" type="text/css">
  </head>
<body>
    <!-- Start: HEADER -->
    <header>
      <!-- Start: Navigation wrapper -->
      <div class="navbar navbar-fixed-top">
        <div class="navbar-inner">
          <div class="container">
            <a href="index.php" class="brand brand-bootbus">Lulworth Cove Drama Club</a>
            <!-- Below button used for responsive navigation -->
            <button type="button" class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <!-- Start: Primary navigation -->
            <div class="nav-collapse collapse">        
              <ul class="nav pull-right">
                <li><a href="manageEvent.php">Manage Events</a></li>
                <li><a href="manageBlog.php">Manage Blog</a></li>
                <li><a href="manageMembers.php">Manage Members</a></li>
                <li><a href="signout.php">Signout</a></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
      <!-- End: Navigation wrapper -->   
    </header>
<!-- End: HEADER -->
    <!-- Start: MAIN CONTENT -->
    <div class="content">
      <!-- Start: slider -->
      <div class="slider">
        <div class="container-fluid">
          <div id="heroSlider" class="carousel slide">
            <div class="carousel-inner">
              <div class="active item">
                <div class="hero-unit">
                  <div class="row-fluid">
                    <div class="span7 marketting-info">
                      <h1>LCDC / RECREATION</h1>
                      <p>
                        Lulworth Cove Drama Club is a recreational club where you enhance you skill on singing, dancing and performing drama on the stage.
                      </p>
                      <h3>
                        <a href="#" class="btn">Learn more</a>
                      </h3>                      
                    </div>
                    <div class="span5">
                      <img src="img/SampleJumpAndDance.jpg" class="thumbnail" alt="image of mask">
                    </div>
                  </div>                  
                </div>
              </div>
              <div class="item">
                <div class="hero-unit">
                  <div class="row-fluid">
                    <div class="span7 marketting-info">
                      <h1>Dance like you have never danced before</h1>
                      <p>
                        Students learning how to do ballet dance.
                        
                      </p>
                      <h3>
                        <a href="#" class="btn btn-primary">Register now</a>
                        <a href="service.html" class="btn">Learn more</a>
                      </h3>                      
                    </div>
                    <div class="span5">
                      <img src="img/Ballet_Girl.jpg" class="thumbnail" alt="image of dancing">
                    </div>
                  </div>                  
                </div>
              </div>
              <div class="item">
                <div class="hero-unit">
                  <div class="row-fluid">
                    <div class="span7 marketting-info">
                      <h1>Perform on stage</h1>
                      <p>
                        Students of Lulworth Cove Drama Club participating in an event.
                        They are performing in a singing competition.
                      </p>
                      <h3>
                        <a href="#" class="btn btn-primary">Register now</a>
                        <a href="product.html" class="btn">Learn more</a>
                      </h3>                      
                    </div>
                    <div class="span5">
                      <img src="img/singing1.jpg" class="thumbnail" alt="image of singing">
                    </div>
                  </div>                  
                </div>
              </div>
            </div>
            <a class="left carousel-control" href="#heroSlider" data-slide="prev">‹</a>
            <a class="right carousel-control" href="#heroSlider" data-slide="next">›</a>
          </div>
        </div>
      </div>
      <!-- End: slider -->
      <!-- Start: About US, Classes, Blog and Events -->
        <div class="container">
            <div class="row-fluid">    
            <div class="page-header">
                <h2>About Us</h2>
                <p id="aboutus">Lulworth Cove Drama Club was formed in 2009 and since that time members have presented a large mass of productions. The club is actively involved in teaching different classes such as drama, singing and dancing.The club's building was originally a school where its most famous pupil, Stan Laurel of “Laurel and Hardy”, attended. Appropriate, perhaps, then that it should become a theatre.
                With the help of members a stage was built, lights were fitted, curtains were made, a dressing room equipped and the little theatre, in which members take great pride, came into being. After 1961 the club became affiliated to the Village Hall complex.
                Improvements are regularly made. In 1983, tip-up seats, obtained from the Darlington Civic Theatre, were installed and financed by members and friends, whose names are on the seats. The most recent development has been a Green Room, which has greatly added to the facilities backstage. Lighting and audio equipment have also been updated.
                There are usually Spring and Autumn productions performed to our many loyal supporters and we have regular play readings and social events for members and friends. The Club’s success comes down to our dedicated members and a wide range of talented helpers who all give their valuable time to the Club’s activities, and to the ever growing audience who support us.
                We hope you enjoy the Club as much as we do.</p>
                </div>
        </div>
          <div class="page-header">
            <h2>Our Classes</h2>
          </div>
          <div class="row-fluid">
            <ul class="thumbnails">
              <li class="span4">
                <div class="thumbnail">
                  <img src="img/Dancing_360_200.jpg" alt="product name">
                  <div class="caption">
                    <h3>Lets Dance</h3>
                    <p>
                      Few attractive words about your product.Few attractive words about your product.
                      Few attractive words about your product.Few attractive words about your product.
                    </p>
                  </div>
                  <div class="widget-footer">
                    <p>
                      <a href="signup.php" class="btn btn-primary" value="registerDance">Register now</a>&nbsp;
                      <a href="product.html" class="btn">Read more</a>
                    </p>
                  </div>
                </div>
              </li>
              <li class="span4">
                <div class="thumbnail">
                  <img src="img/Stage_360_200.jpg" alt="product name">
                  <div class="caption">
                    <h3>Lets Perform</h3>
                    <p>
                      Few attractive words about your product.Few attractive words about your product.
                      Few attractive words about your product.Few attractive words about your product.
                    </p>
                  </div>
                  <div class="widget-footer">
                    <p>
                      <a href="signup.php" class="btn btn-primary" value="registerDrama">Register now</a>&nbsp;
                      <a href="product.html" class="btn">Read more</a>
                    </p>
                  </div>
                </div>
              </li>
              <li class="span4">
                <div class="thumbnail">
                  <img src="img/singing_360_200.jpg" alt="product name">
                  <div class="caption">
                    <h3>Lets sing</h3>
                    <p>
                      Few attractive words about your product.Few attractive words about your product.
                      Few attractive words about your product.Few attractive words about your product.
                    </p>
                  </div>
                  <div class="widget-footer">
                    <p>
                      <a href="signup.php" class="btn btn-primary" value="registerSing">Register now</a>&nbsp;
                      <a href="product.html" class="btn">Read more</a>
                    </p>
                  </div>
                </div>
              </li>
            </ul>
          </div>
          <div class="page-header">
            <h2>Our Events | Blogs</h2>
          </div>
          <div class="row-fluid">
            <ul class="thumbnails">
              <li class="span4">
                <div class="thumbnail">
                  <img src="img/StageDance_360_200.jpg" alt="product name">
                  <div class="caption">
                    <h3>Dance Competition</h3>
                    <p>
                      Few attractive words about your service.Few attractive words about your service.
                      Few attractive words about your service.Few attractive words about your service.
                    </p>
                  </div>
                  <div class="widget-footer">
                    <p>
                      <a href="index.php" class="btn btn-primary" onClick="Javascript: alert('Successfully Saved')">Save</a>&nbsp;
                      <a href="product.html" class="btn">Read More</a>
                    </p>
                  </div>
                </div>
              </li>
              <li class="span4">
                <div class="thumbnail">
                  <img src="img/Singing_360_200_2.jpg" alt="product name">
                  <div class="caption">
                    <h3>Singing Competition</h3>
                    <p>
                      Few attractive words about your service.Few attractive words about your service.
                      Few attractive words about your service.Few attractive words about your service.
                    </p>
                  </div>
                  <div class="widget-footer">
                    <p>
                      <a href="index.php" class="btn btn-primary" onClick="Javascript: alert('Successfully Saved')">Save</a>&nbsp;
                      <a href="product.html" class="btn">Read more</a>
                    </p>
                  </div>
                </div>
              </li>
              <li class="span4">
                <div class="thumbnail">
                  <img src="img/Drama_competition.jpg" alt="product name">
                  <div class="caption">
                    <h3>Drama Competition</h3>
                    <p>
                      Few attractive words about your service.Few attractive words about your service.
                      Few attractive words about your service.Few attractive words about your service.
                    </p>
                  </div>
                  <div class="widget-footer">
                    <p>
                      <a href="index.php" class="btn btn-primary" onClick="Javascript: alert('Successfully Saved')">Save</a>&nbsp;
                      <a href="product.html" class="btn">Read more</a>
                    </p>
                  </div>
                </div>
              </li>
            </ul>
          </div>
            <div class="page-header">
            <h2>Reviews about LCDC</h2>
          </div>
          <div class="row-fluid">
            <ul class="thumbnails">
              <li class="span3">
                <div class="thumbnail">
                  <img src="img/StageDance_360_200.jpg" alt="product name">
                  <div class="caption">
                    <h3>Dance Competition</h3>
                    <p>
                      Few attractive words about your service.Few attractive words about your service.
                      Few attractive words about your service.Few attractive words about your service.
                    </p>
                  </div>
                </div>
              </li>
              <li class="span3">
                <div class="thumbnail">
                  <img src="img/Singing_360_200_2.jpg" alt="product name">
                  <div class="caption">
                    <h3>Singing Competition</h3>
                    <p>
                      Few attractive words about your service.Few attractive words about your service.
                      Few attractive words about your service.Few attractive words about your service.
                    </p>
                  </div>
                </div>
              </li>
              <li class="span3">
                <div class="thumbnail">
                  <img src="img/Drama_competition.jpg" alt="product name">
                  <div class="caption">
                    <h3>Drama Competition</h3>
                    <p>
                      Few attractive words about your service.Few attractive words about your service.
                      Few attractive words about your service.Few attractive words about your service.
                    </p>
                  </div>
                </div>
              </li>
                <li class="span3">
                <div class="thumbnail">
                  <img src="img/Drama_competition.jpg" alt="product name">
                  <div class="caption">
                    <h3>Drama Competition</h3>
                    <p>
                      Few attractive words about your service.Few attractive words about your service.
                      Few attractive words about your service.Few attractive words about your service.
                    </p>
                  </div>
                </div>
              </li>
            </ul>
          </div>
        </div>
      <!-- End: About US, Classes, Blog and Events -->
    </div>
<!-- End: MAIN CONTENT -->
<?php include_once("include/footer.php"); ?>