<form method="POST">
	Click <button name='submit'>Proceed</button>  to set up database.<br />
</form>

<?php
if(isset($_POST['submit'])){
	$con = new mysqli("localhost","root","");
	$qry_db="CREATE DATABASE IF NOT EXISTS `dbWW`";

	if($con->query($qry_db)){
		echo "Database successfuly created! <br/>";
		$con = new mysqli("localhost","root","","dbWW");
		echo "Database 'dbsas' connected!<br /><br/>";
	} else {
		echo "Error while creating database: ".$con->error_connect;
	}

	//Table structure for table `counter`
	$tbl_counter= "CREATE TABLE `counter` (
		  `visit_count` int(11) NOT NULL
		)";
	if($con->query($tbl_counter)){
		echo "Table 'counter' successfuly created!<br/>";
	} else {
		echo "Error: ".$con->error."<br/>";
	}

	//Table structure for table `users`
	$tbl_users= "CREATE TABLE `users` (
	  `uid` int(11) NOT NULL AUTO_INCREMENT,
	  `name` varchar(100) NOT NULL,
	  `gender` varchar(10) NOT NULL,
	  `dob` date NOT NULL,
	  `phone` varchar(20) NOT NULL,
	  `email` varchar(50) NOT NULL,
	  `postal_addr` varchar(50) NOT NULL,
	  `postal_code` int(11) NOT NULL,
	  `username` varchar(20) NOT NULL,
	  `password` varchar(25) NOT NULL,
	  `type` varchar(20) NOT NULL,
	  `photo` varchar(100) NOT NULL,
	  `last_login` date NOT NULL,
	  PRIMARY KEY (uid),
	  UNIQUE(username)
	)";
	if($con->query($tbl_users)){
		echo "Table 'users' successfuly created!<br/>";
	} else {
		echo "Error: ".$con->error."<br/>";
	}

	//Table structure for table `pets`
	$tbl_pets= "CREATE TABLE `pets` (
	  `pid` int(11) NOT NULL AUTO_INCREMENT,
	  `type` varchar(4) NOT NULL,
	  `name` varchar(50) NOT NULL,
	  `breed` varchar(50) NOT NULL,
	  `image` varchar(50) NOT NULL,
	  PRIMARY KEY (pid)
	)";
	if($con->query($tbl_pets)){
		echo "Table 'pets' successfuly created!<br/>";
	} else {
		echo "Error: ".$con->error."<br/>";
	}

	//Table structure for table `adoption`
	$tbl_adoption= "CREATE TABLE `adoption` (
	  `ad_id` int(11) NOT NULL AUTO_INCREMENT,
	  `pid` int(11) NOT NULL,
	  `uid` int(11) NOT NULL,
	  `user` varchar(50) NOT NULL,
	  PRIMARY KEY (ad_id)
	)";
	if($con->query($tbl_adoption)){
		echo "Table 'adoption' successfuly created!<br/>";
	} else {
		echo "Error: ".$con->error."<br/>";
	}

	//Table structure for table `donation`
	$tbl_donation="CREATE TABLE `donation` (
	  `d_id` int(11) NOT NULL AUTO_INCREMENT,
	  `uid` int(11) NOT NULL,
	  `username` varchar(50) NOT NULL,
	  `date` varchar(10) NOT NULL,
	  `amount` int(11) NOT NULL,
	  PRIMARY KEY (d_id)
	)";
	if($con->query($tbl_donation)){
		echo "Table 'users' successfuly created!<br/>";
	} else {
		echo "Error: ".$con->error."<br/>";
	}
	
	//Table structure for table `questions`
	$tbl_questions="CREATE TABLE `questions` (
	  `que_id` int(11) NOT NULL AUTO_INCREMENT,
	  `uid` int(11) NOT NULL,
	  `question` varchar(500) NOT NULL,
	  `user` varchar(50) NOT NULL,
	  PRIMARY KEY (que_id)
	)";
	if($con->query($tbl_questions)){
		echo "Table 'users' successfuly created!<br/>";
	} else {
		echo "Error: ".$con->error."<br/>";
	}

	//Table structure for table `answers`
	$tbl_answers= "CREATE TABLE `answers` (
	  `ans_id` int(11) NOT NULL AUTO_INCREMENT,
	  `qid` int(11) NOT NULL,
	  `ans` varchar(500) NOT NULL,
	  `uid` int(11) NOT NULL,
	  `user` varchar(50) NOT NULL,
	  PRIMARY KEY (ans_id)
	)";
	if($con->query($tbl_answers)){
		echo "Table 'users' successfuly created!";
	} else {
		echo "Error: ".$con->error;
	}
    
    //Creating an administrative user.
    $today = date("Y-m-d");
    INSERT INTO `tbl_register` (`id`,`fullname`,`email`,`dob`,`address`,`username`,`password`,`type`,`last_login`) VALUES('', 'admin', 'admin@admin.com', $today, 'admin', 'admin', 'admin', 'admin', $today);
}

?>