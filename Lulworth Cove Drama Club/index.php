<?php include_once("include/header.php");

require 'dbConnect.php';

?>
<!-- Start: MAIN CONTENT -->
    <div class="content">
      <!-- Start: slider -->
      <div class="slider">
        <div class="container-fluid">
          <div id="heroSlider" class="carousel slide">
            <div class="carousel-inner">
              <div class="active item">
                <div class="hero-unit">
                  <div class="row-fluid">
                    <div class="span7 marketting-info">
                      <h1>LCDC / RECREATION</h1>
                      <p>
                        Lulworth Cove Drama Club is a recreational club where you enhance you skill on singing, dancing and performing drama on the stage.
                      </p>
                      <h3>
                        <a href="#" class="btn">Learn more</a>
                      </h3>                      
                    </div>
                    <div class="span5">
                      <img src="img/SampleJumpAndDance.jpg" class="thumbnail" alt="image of mask">
                    </div>
                  </div>                  
                </div>
              </div>
              <div class="item">
                <div class="hero-unit">
                  <div class="row-fluid">
                    <div class="span7 marketting-info">
                      <h1>Dance like you have never danced before</h1>
                      <p>
                        Students learning how to do ballet dance.
                        
                      </p>
                      <h3>
                        <a href="signup.php" class="btn btn-primary">Register now</a>
                        <a href="service.html" class="btn">Learn more</a>
                      </h3>                      
                    </div>
                    <div class="span5">
                      <img src="img/Ballet_Girl.jpg" class="thumbnail" alt="image of dancing">
                    </div>
                  </div>                  
                </div>
              </div>
              <div class="item">
                <div class="hero-unit">
                  <div class="row-fluid">
                    <div class="span7 marketting-info">
                      <h1>Perform on stage</h1>
                      <p>
                        Students of Lulworth Cove Drama Club participating in an event.
                        They are performing in a singing competition.
                      </p>
                      <h3>
                        <a href="signup.php" class="btn btn-primary">Register now</a>
                        <a href="product.html" class="btn">Learn more</a>
                      </h3>                      
                    </div>
                    <div class="span5">
                      <img src="img/singing1.jpg" class="thumbnail" alt="image of singing">
                    </div>
                  </div>                  
                </div>
              </div>
            </div>
            <a class="left carousel-control" href="#heroSlider" data-slide="prev">‹</a>
            <a class="right carousel-control" href="#heroSlider" data-slide="next">›</a>
          </div>
        </div>
      </div>
      <!-- End: slider -->
      <!-- Start: About US, Classes, Blog and Events -->
        <div class="container">
            <div class="row-fluid">
            <div class="page-header">
                <h2>About Us</h2>
                <p id="aboutus">Lulworth Cove Drama Club was formed in 2009 and since that time members have presented a large mass of productions. The club is actively involved in teaching different classes such as drama, singing and dancing.The club's building was originally a school where its most famous pupil, Stan Laurel of “Laurel and Hardy”, attended. Appropriate, perhaps, then that it should become a theatre.
                With the help of members a stage was built, lights were fitted, curtains were made, a dressing room equipped and the little theatre, in which members take great pride, came into being. After 1961 the club became affiliated to the Village Hall complex.
                Improvements are regularly made. In 1983, tip-up seats, obtained from the Darlington Civic Theatre, were installed and financed by members and friends, whose names are on the seats. The most recent development has been a Green Room, which has greatly added to the facilities backstage. Lighting and audio equipment have also been updated.
                There are usually Spring and Autumn productions performed to our many loyal supporters and we have regular play readings and social events for members and friends. The Club’s success comes down to our dedicated members and a wide range of talented helpers who all give their valuable time to the Club’s activities, and to the ever growing audience who support us.
                We hope you enjoy the Club as much as we do.</p>
                </div>
        </div>
          <div class="page-header">
            <h2>Our Classes</h2>
          </div>
          <div class="row-fluid">
            <ul class="thumbnails">
              <li class="span4">
                <div class="thumbnail">
                  <img src="img/Dancing_360_200.jpg" alt="product name">
                  <div class="caption">
                    <h3>Lets Dance</h3>
                    <p>
                      Lulworth Cove Drama Club provides excellent course on dance. LCDC has excellent,
                        experienced teachers to make the children level up their position.
                    </p>
                  </div>
                  <div class="widget-footer">
                    <p>
                      <a href="signup.php" class="btn btn-primary" value="registerDance">Register now</a>&nbsp;
                      <a href="product.html" class="btn">Read more</a>
                    </p>
                  </div>
                </div>
              </li>
              <li class="span4">
                <div class="thumbnail">
                  <img src="img/Stage_360_200.jpg" alt="product name">
                  <div class="caption">
                    <h3>Lets Perform</h3>
                    <p>
                      Lulworth Cove Drama Club organizes various events and competition on 
                        performing drama on the stage.
                    </p>
                  </div>
                  <div class="widget-footer">
                    <p>
                      <a href="signup.php" class="btn btn-primary" value="registerDrama">Register now</a>&nbsp;
                      <a href="product.html" class="btn">Read more</a>
                    </p>
                  </div>
                </div>
              </li>
              <li class="span4">
                <div class="thumbnail">
                  <img src="img/singing_360_200.jpg" alt="product name">
                  <div class="caption">
                    <h3>Lets sing</h3>
                    <p>
                      Lulworth Cove Drama Club offers high level of singing course for children with
                        the help of friendly environment and experienced teachers.
                    </p>
                  </div>
                  <div class="widget-footer">
                    <p>
                      <a href="signup.php" class="btn btn-primary" value="registerSing">Register now</a>&nbsp;
                      <a href="product.html" class="btn">Read more</a>
                    </p>
                  </div>
                </div>
              </li>
            </ul>
          </div>
          <div class="page-header">
            <h2>Our Events | Blogs</h2>
          </div>
          <div class="row-fluid">
            <ul class="thumbnails">
              <li class="span4">
                <div class="thumbnail">
                  <img src="img/StageDance_360_200.jpg" alt="product name">
                  <div class="caption">
                    <h3>Dance Competition</h3>
                    <p>
                      Students of Lulworth Cove Drama Club participating in the dance competition 
                        held by Level Up Club.
                    </p>
                  </div>
                  <div class="widget-footer">
                    <p>
                      <a href="index.php" class="btn btn-primary" onClick="Javascript: alert('Successfully Saved')">Save</a>&nbsp;
                      <a href="product.html" class="btn">Read More</a>
                    </p>
                  </div>
                </div>
              </li>
              <li class="span4">
                <div class="thumbnail">
                  <img src="img/Singing_360_200_2.jpg" alt="product name">
                  <div class="caption">
                    <h3>Singing Competition</h3>
                    <p>
                      Lulworth Cove Drama Club organizing singing competition. A lot of other 
                        club have participated in the competition.
                    </p>
                  </div>
                  <div class="widget-footer">
                    <p>
                      <a href="index.php" class="btn btn-primary" onClick="Javascript: alert('Successfully Saved')">Save</a>&nbsp;
                      <a href="product.html" class="btn">Read more</a>
                    </p>
                  </div>
                </div>
              </li>
              <li class="span4">
                <div class="thumbnail">
                  <img src="img/Drama_competition.jpg" alt="product name">
                  <div class="caption">
                    <h3>Drama Competition</h3>
                    <p>
                      Level Up Club organized a Drama Competition. Lulworth Cove Drama Club becomes second in the competition.
                    </p>
                  </div>
                  <div class="widget-footer">
                    <p>
                      <a href="index.php" class="btn btn-primary" onClick="Javascript: alert('Successfully Saved')">Save</a>&nbsp;
                      <a href="product.html" class="btn">Read more</a>
                    </p>
                  </div>
                </div>
              </li>
            </ul>
          </div>
        </div>
      <!-- End: About US, Classes, Blog and Events -->
    </div>
<!-- End: MAIN CONTENT -->
<?php include_once("include/footer.php");  ?>